const chai = require('chai')
const http = require('chai-http')
const expect = chai.expect
chai.use(http)

describe("api_test_suite", () => {
    it('test_api_people_is_running', () => {
        chai.request('http://localhost:4000')
        .get('/people')
        .end((err, res) => {
            expect(res).to.not.equal(undefined)
        })
    })

    it('test_api_people_returns_200', (done) => {
        chai.request('http://localhost:4000')
        .get('/people')
        .end((err, res) => {
            expect(res.status).to.equal(200)
            done()
        })
    })

    it('test_api_post_person_returns_400_if_no_person_name', (done) => {
        chai.request('http://localhost:4000')
        .post('/person')
        .type('json')
        .send({
            alias: 'Jason',
            age: 28
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done()
        })
    })
})

describe('api_test_suite_users', () => {
    it('test_api_post_person_is_running', () => {
        chai.request('http://localhost:4000')
        .post('/person')
        .type('json')
        .send({
            username: 'us3rn@m3',
            name: 'Mark',
            age: 26
        })
        .end((err, res) => {
            expect(res.status).to.equal(200)
        })
    })

    it('test_api_post_person_returns_400_if_no_username', () => {
        chai.request('http://localhost:4000')
        .post('/person')
        .type('json')
        .send({
            name: 'Mark',
            age: 26
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
        })
    })

    it('test_api_post_person_returns_400_if_no_age', () => {
        chai.request('http://localhost:4000')
        .post('/person')
        .type('json')
        .send({
            username: 'us3rn@m3',
            name: 'Mark'
        })
    })
})