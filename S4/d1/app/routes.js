const { names, users } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    })

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
        }
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
        }
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            })
        }
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            })	
        }

        // Add a another if statement that returns and sends 400 status and error message if the request body does not have a username field.
         if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error' : 'Bad Request: Alias is missing property'
            })
        }
    })

    // S4 Activity Template START
    app.post('/login',(req,res)=>{

        let foundUser = users.find((user) => {

            return user.username === req.body.username && user.password === req.body.password

        });

        if(!req.body.hasOwnProperty('username')){
            // Add code here to pass the test
            return res.status(400).send({
                success: false,
                error: 'You need to provide username'
            })
        }

        if(!req.body.hasOwnProperty('password')){
            // Add code here to pass the test
            return res.status(400).send({
                success: false,
                error: 'You need to provided password'
            })
        }

        if(foundUser){
            // Add code here to pass the test
            return res.send({
                success: true,
                message: 'You have successfully logged in'
            })
        }

        // Stretch goal
        if(!foundUser){
            return res.status(403).send({
                success: false,
                message: 'Please check that your username and password are correct'
            })
        }
    })
    // S4 Activity Template END
}